The shell, holding various plugins

# Local development
Clone repo
`npm install`
`npm run build`
`npm start`
Visit `http://localhost:8000`, should see a box, nothing in it.

# Build
You don't need to build, if you are in local development. It's only when you deploy.
`npm run build` or `npm run build-prod`

# See a plugin
When the shell is running, once the plugin's js code is available at the URL you mentioned in `Shell.js` and/or `shell-service.js`, you will see the plugin's presence within the shell, under `div#shell`

# Add a new plugin
Develop, build your new plugin in isolation. Once ready, build it, then have its `.js` file available at some URL.
In `Shell.js` and/or `shell-service.js`, add code to load the js file of this new plugin.

