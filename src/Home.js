import React from "react";
import { Link } from 'react-router-dom';

class Home extends React.Component {
 
  render() {
    return(
      <div className="home">
        <p>Home contents: main layout, menu, global links, etc.</p>
        {/* faked main menu */}
        <Link to="/app1">App one</Link> (deployed)<br />
        <Link to="/app2">App two</Link> (deployed)<br />
        <Link to="/appX">App X</Link> (not exist)<br />
      </div>
    );
  }
}

export default Home;