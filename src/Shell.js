import React from "react";
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './Home';
import "./Shell.css";

const appsAlreadyLoaded = []; // to prevent duplicate loads

class Shell extends React.Component {

  render() {
    return(
      <div className="shell" id="shell">
        {/*  empty shell */}

        {/* app routings */}
        <BrowserRouter>
          <Route path="/" exact component={HomeFresh} />
          <Route path="/app1" component={App1} />
          <Route path="/app2" component={App2} />
          <Route path="/appx" render={() => null} />
        </BrowserRouter>
      </div>
    );
  }
}

// TODO hide the plugin DIVs is fine, but it's better 
// to reload the whole page. Find out how.

const HomeFresh = () => {
  const plugins = document.querySelectorAll('.shell .plugin');
  plugins.forEach((plugin) => {
    plugin.classList.add('hidden');
  });
  return <Home />;
}

const App1 = () => {
  // Load plugin #1, example here is 'loading unconditionally'
  return loadJS('pi-1', 'http://localhost:8080/cdn/js/pi-1/1.0.1/pi-1.js');
}

const App2 = () => {
  // Load plugin #2, example here is loading on some conditions, such as user
  if (isAppEligible({ appId: 'pi-2', userId: 'good-guy' })) {
    return loadJS('pi-2', 'http://localhost:8080/cdn/js/pi-2/1.0.2/pi-2.js');
  }
  return null;
}

// TODO: it's not scaling well here when you have new apps
// Let's try HOC, or a function, to generate App_N





//////////////////////////////////////
// TODO move these to a util

const loadJS = (appId, jsFile) => {
  if (appsAlreadyLoaded.includes(appId)) {
    document.getElementById(appId).classList.remove('hidden');
    return null;
  }
  const s = document.createElement("script");
  s.setAttribute("type", "text/javascript");
  s.setAttribute("defer", true);
  s.setAttribute("src", jsFile);
  document.body.appendChild(s);
  appsAlreadyLoaded.push(appId);
  return null;
}

const isAppEligible = ({ appId, userId} ) => {
  // read data from backend, plugin config, etc. to determine if user can access app
  const isEligible = userId === 'good-guy';
  console.log('User ', userId, ' can access app ', appId, '? ---> ', isEligible);
  return isEligible;
}

export default Shell;