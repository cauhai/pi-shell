import React from "react";
import ReactDOM from "react-dom";
import Shell from "./src/Shell.js";
import "./shell-service";

ReactDOM.render(<Shell />, document.getElementById("shell-container"));
